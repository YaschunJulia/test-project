## Installation

Run `npm install` to install all dependencies.

## Running project

Run `npm start` to open project in the browser.