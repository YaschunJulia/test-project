import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from "@angular/http";
import { FormsModule } from "@angular/forms";

import { InMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService }  from './services/in-memory-data.service';

import { AppComponent }   from './app.component';
import { BreadCrumbsModule }   from './components/bread_crumbs/bread-crumbs.module';

import { AppRoutingModule } from './app-routing.module';

import { UserService } from './services/user.service';

@NgModule({
  imports:      [
    BrowserModule,
    HttpModule,
    FormsModule,
    InMemoryWebApiModule.forRoot(InMemoryDataService),
    AppRoutingModule,
    BreadCrumbsModule],
  declarations: [
    AppComponent,
  ],
  bootstrap:    [ AppComponent ],
  providers:    [ UserService],
})
export class AppModule { }