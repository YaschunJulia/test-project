import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', loadChildren: 'app/components/simple/simple.module#SimpleModule'},
      {
        path: 'dashboard',
        loadChildren: 'app/components/simple/simple.module#SimpleModule'
      },
      {
        path: 'messages',
        loadChildren: 'app/components/simple/simple.module#SimpleModule'
      },
      {
        path: 'my-cars',
        loadChildren: 'app/components/simple/simple.module#SimpleModule'
      },
      {
        path: 'active-queries',
        loadChildren: 'app/components/active_query/active-query.module#ActiveQueryModule'
      },
      {
        path: 'completed-services',
        loadChildren: 'app/components/simple/simple.module#SimpleModule'
      },
      {
        path: 'deleted-queries',
        loadChildren: 'app/components/simple/simple.module#SimpleModule'
      }
  ];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})

export class AppRoutingModule {}