"use strict";
const testing_1 = require("@angular/core/testing");
const platform_browser_1 = require("@angular/platform-browser");
const http_1 = require("@angular/http");
const forms_1 = require("@angular/forms");
const app_component_1 = require("./app.component");
let comp;
let fixture;
describe('AppComponent & AdditionalComponent', function () {
    beforeEach(testing_1.async(() => {
        testing_1.TestBed.configureTestingModule({
            declarations: [app_component_1.AppComponent],
            providers: [],
            imports: [platform_browser_1.BrowserModule, http_1.HttpModule, forms_1.FormsModule],
        })
            .compileComponents().then(createComponent);
    }));
});
function createComponent() {
    fixture = testing_1.TestBed.createComponent(app_component_1.AppComponent);
    comp = fixture.componentInstance;
    fixture.detectChanges();
    return fixture.whenStable().then(() => {
        fixture.detectChanges();
    });
}
//# sourceMappingURL=app.component.spec.js.map