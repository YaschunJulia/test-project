"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
const core_1 = require("@angular/core");
const router_1 = require("@angular/router");
const links_1 = require("./models/links");
const user_service_1 = require("./services/user.service");
let AppComponent = class AppComponent {
    constructor(router, e, userService) {
        this.router = router;
        this.e = e;
        this.userService = userService;
        this.menu = {
            navbarItemOpen: false,
            sidebarItemOpen: false,
            sidebarOpen: true,
        };
        this.links = links_1.LINKS;
    }
    ngOnInit() {
        this.getData();
        this.checkWindowWidth();
        if (document.documentElement.clientWidth < 720) {
            this.menu['sidebarOpen'] = false;
            this.menu['sidebarItemOpen'] = false;
            this.mobileSize = true;
        }
    }
    // Get user data from UserService
    getData() {
        this.userService.getUserInfo()
            .then((user) => this.user = user)
            .catch(() => { });
    }
    // Change sidebar and main style when response page
    checkWindowWidth() {
        let main = this.e.nativeElement.querySelector('.main');
        let open = this.e.nativeElement.querySelector('.open').parentNode;
        let close = this.e.nativeElement.querySelector('.close').parentNode;
        window.onresize = () => {
            if (document.documentElement.clientWidth < 720) {
                this.menu['sidebarOpen'] = false;
                this.menu['sidebarItemOpen'] = false;
                this.mobileSize = true;
                main.classList.add('resize');
                open.classList.remove('hidden');
                close.classList.remove('show');
                open.classList.add('show');
                close.classList.add('hidden');
            }
            else {
                this.menu['sidebarOpen'] = true;
                this.mobileSize = false;
                main.classList.remove('resize');
                open.classList.remove('show');
                close.classList.remove('hidden');
                open.classList.add('hidden');
                close.classList.add('show');
            }
        };
    }
    //Close dropdown when click outside the element
    closeDropdown() {
        document.body.onclick = (event) => {
            if (event.target.className != 'dropdown-menu-item' && event.target.className.indexOf('dropdown-open') === -1) {
                this.menu['navbarItemOpen'] = false;
            }
        };
    }
    //Change basic constants for open and close menu
    menuOperation(item) {
        this.menu[item] = !this.menu[item];
        if (this.menu['navbarItemOpen'] = true) {
            this.closeDropdown();
        }
    }
};
AppComponent = __decorate([
    core_1.Component({
        selector: 'my-app',
        templateUrl: 'app/app.component.html',
        styleUrls: ['app/app.component.css']
    }),
    __metadata("design:paramtypes", [router_1.Router,
        core_1.ElementRef,
        user_service_1.UserService])
], AppComponent);
exports.AppComponent = AppComponent;
//# sourceMappingURL=app.component.js.map