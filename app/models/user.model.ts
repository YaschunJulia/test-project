/**
 * Created by yaschun on 07.04.17.
 */
export class User {
    constructor(
        public name: string,
        public count: number,
        public bonus: string
    ){}

    clone() {
        return new User(this.name, this.count, this.bonus);
    }

}