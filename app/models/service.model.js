"use strict";
/**
 * Created by yaschun on 07.04.17.
 */
class Service {
    constructor(id, name, count, deleted, status, description, activateFrom, activateTo, comment) {
        this.id = id;
        this.name = name;
        this.count = count;
        this.deleted = deleted;
        this.status = status;
        this.description = description;
        this.activateFrom = activateFrom;
        this.activateTo = activateTo;
        this.comment = comment;
    }
    clone() {
        return new Service(this.id, this.name, this.count, this.deleted, this.status, this.description, this.activateFrom, this.activateTo, this.comment);
    }
}
exports.Service = Service;
//# sourceMappingURL=service.model.js.map