/**
 * Created by yaschun on 07.04.17.
 */
export class Service {
    constructor(
        public id: number,
        public name: string,
        public count: number,
        public deleted: boolean,
        public status: string,
        public description: string,
        public activateFrom: Date,
        public activateTo: Date,
        public comment: string,
    ){}

    clone() {
        return new Service(this.id, this.name, this.count, this.deleted, this.status, this.description, this.activateFrom,
            this.activateTo, this.comment);
    }

}