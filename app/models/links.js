"use strict";
/**
 * Created by yaschun on 07.04.17.
 */
exports.LINKS = [
    { title: 'Старт', link: '/home' },
    { title: 'Приборная панель', link: '/dashboard' },
    { title: 'Сообщения', link: '/messages' },
    { title: 'Мои автомобили', link: '/my-cars' },
    { title: 'Активные запросы', link: '/active-queries' },
    { title: 'Завершенные услуги', link: '/completed-services' },
    { title: 'Удаленные запросы', link: '/deleted-queries' },
    { title: 'Новая Услуга', link: '/active-queries/create-new' },
];
//# sourceMappingURL=links.js.map