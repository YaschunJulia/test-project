"use strict";
/**
 * Created by yaschun on 07.04.17.
 */
class User {
    constructor(name, count, bonus) {
        this.name = name;
        this.count = count;
        this.bonus = bonus;
    }
    clone() {
        return new User(this.name, this.count, this.bonus);
    }
}
exports.User = User;
//# sourceMappingURL=user.model.js.map