import { Component, OnInit, ElementRef} from '@angular/core';
import { Router } from '@angular/router';

import { LINKS } from './models/links'
import { User } from './models/user.model'

import { UserService } from './services/user.service'

@Component({
  selector: 'my-app',
  templateUrl: 'app/app.component.html',
  styleUrls: ['app/app.component.css']
})

export class AppComponent implements OnInit {
  user: User;
  mobileSize: boolean;
  menu: Object = {
    navbarItemOpen: false,
    sidebarItemOpen: false,
    sidebarOpen:  true,
  };
  links: Object[] = LINKS;

  constructor (
    private router: Router,
    private e: ElementRef,
    private userService: UserService) {}

  ngOnInit() {
    this.getData();
    this.checkWindowWidth();
    if( document.documentElement.clientWidth < 720)
    {
      this.menu['sidebarOpen'] = false;
      this.menu['sidebarItemOpen'] = false;
      this.mobileSize = true;
    }
  }

  // Get user data from UserService
  private getData() {
    this.userService.getUserInfo()
        .then( (user: User) => this.user = user)
        .catch(() => {})
  }

  // Change sidebar and main style when response page
  private checkWindowWidth() {
    let main = this.e.nativeElement.querySelector('.main');
    let open = this.e.nativeElement.querySelector('.open').parentNode;
    let close = this.e.nativeElement.querySelector('.close').parentNode;
    window.onresize = () => {
      if( document.documentElement.clientWidth < 720)
      {
        this.menu['sidebarOpen'] = false;
        this.menu['sidebarItemOpen'] = false;
        this.mobileSize = true;
        main.classList.add('resize');
        open.classList.remove('hidden');
        close.classList.remove('show');
        open.classList.add('show');
        close.classList.add('hidden');
      } else {
        this.menu['sidebarOpen'] = true;
        this.mobileSize = false;
        main.classList.remove('resize');
        open.classList.remove('show');
        close.classList.remove('hidden');
        open.classList.add('hidden');
        close.classList.add('show');
      }
    }
  }

  //Close dropdown when click outside the element
  private closeDropdown() {
    document.body.onclick = (event: any) => {
      if (event.target.className != 'dropdown-menu-item' && event.target.className.indexOf('dropdown-open') === -1)
      {
        this.menu['navbarItemOpen'] = false;
      }
    }
  }

  //Change basic constants for open and close menu
  menuOperation(item : string) {
    this.menu[item] = !this.menu[item];
    if (this.menu['navbarItemOpen'] = true) {
      this.closeDropdown();
    }
  }

}