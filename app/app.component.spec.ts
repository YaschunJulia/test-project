import { ComponentFixture, TestBed, async} from '@angular/core/testing';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from "@angular/http";
import { FormsModule } from "@angular/forms";

import { AppComponent } from './app.component';

let comp: AppComponent;
let fixture: ComponentFixture<AppComponent>;

describe('AppComponent & AdditionalComponent', function () {
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [AppComponent],
            providers: [],
            imports: [BrowserModule, HttpModule, FormsModule],
        })
            .compileComponents().then(createComponent);
    }));
});

function createComponent() {
    fixture = TestBed.createComponent(AppComponent);
    comp = fixture.componentInstance;

    fixture.detectChanges();

    return fixture.whenStable().then(() => {
        fixture.detectChanges();
    });
}
