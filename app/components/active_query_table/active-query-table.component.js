"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
const core_1 = require("@angular/core");
const router_1 = require("@angular/router");
const ng2_bs3_modal_1 = require("ng2-bs3-modal/ng2-bs3-modal");
const queries_service_1 = require("../../services/queries.service");
let ActiveQueryTableComponent = class ActiveQueryTableComponent {
    constructor(router, queriesService) {
        this.router = router;
        this.queriesService = queriesService;
    }
    ngOnInit() {
        this.getData();
    }
    getData() {
        this.queriesService.getServices()
            .then((services) => {
            this.services = services;
        })
            .catch(() => { });
    }
    addField() {
        this.router.navigate(['../active-queries/create-new']);
    }
    editField(service) {
        window.localStorage.setItem('service-name', service.name);
        this.queriesService.serviceName = service.name;
        this.router.navigate(['../active-queries', service.id]);
    }
    deleteField(service) {
        this.currentService = service;
        this.modal.open();
    }
    modalClose() {
        if (this.currentService) {
            this.queriesService.deleteService(this.currentService.id)
                .then(() => {
                this.services = this.services.filter(s => s !== this.currentService);
                this.currentService = null;
                this.modal.close();
            })
                .catch(() => {
                alert('Can\'t delete this service');
                this.modal.close();
            });
        }
    }
};
__decorate([
    core_1.ViewChild('createTemplateModal'),
    __metadata("design:type", ng2_bs3_modal_1.ModalComponent)
], ActiveQueryTableComponent.prototype, "modal", void 0);
ActiveQueryTableComponent = __decorate([
    core_1.Component({
        selector: 'active-query',
        templateUrl: 'app/components/active_query_table/active-query-table.component.html',
        styleUrls: ['app/components/active_query_table/active-query-table.component.css']
    }),
    __metadata("design:paramtypes", [router_1.Router,
        queries_service_1.QueriesService])
], ActiveQueryTableComponent);
exports.ActiveQueryTableComponent = ActiveQueryTableComponent;
//# sourceMappingURL=active-query-table.component.js.map