import { Component, OnInit, ViewChild} from '@angular/core';
import { Router } from '@angular/router';
import { ModalComponent } from 'ng2-bs3-modal/ng2-bs3-modal';

import { QueriesService } from '../../services/queries.service';

import { Service } from '../../models/service.model';

@Component({
  selector: 'active-query',
  templateUrl: 'app/components/active_query_table/active-query-table.component.html',
  styleUrls: ['app/components/active_query_table/active-query-table.component.css']
})

export class ActiveQueryTableComponent implements OnInit{
  services: Service[];
  currentService: Service;

  @ViewChild('createTemplateModal') modal: ModalComponent;

  constructor (
      private router: Router,
      private queriesService: QueriesService
  ) {}

  ngOnInit() {
    this.getData()
  }

  getData() {
    this.queriesService.getServices()
        .then((services: Service[]) => {
          this.services = services;
        })
        .catch( () => {})
  }

  addField() {
    this.router.navigate(['../active-queries/create-new'])
  }

  editField(service: Service) {
    window.localStorage.setItem('service-name', service.name);
    this.queriesService.serviceName = service.name;
    this.router.navigate(['../active-queries', service.id])
  }

  deleteField(service: Service) {
    this.currentService = service;
    this.modal.open();
  }

  modalClose() {
    if (this.currentService) {
      this.queriesService.deleteService(this.currentService.id)
          .then(() => {
            this.services = this.services.filter(s => s !== this.currentService);
            this.currentService = null;
            this.modal.close()
          })
          .catch(() => {
            alert('Can\'t delete this service');
            this.modal.close();
          })
    }
  }


}