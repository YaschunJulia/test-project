/**
 * Created by yaschun on 11.04.17.
 */
import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";
import { HttpModule } from "@angular/http";
import { FormsModule } from "@angular/forms";

import { SimpleComponent } from './simple.component';
import { SimpleRoutingModule } from './simple-routing.module';
import { BreadCrumbsModule }   from '../bread_crumbs/bread-crumbs.module';

@NgModule({
    imports:      [
        CommonModule,
        HttpModule,
        FormsModule,
        SimpleRoutingModule,
        BreadCrumbsModule],
    declarations: [
        SimpleComponent,
    ],
    bootstrap: [ SimpleComponent ],
})
export class SimpleModule { }