"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
const core_1 = require("@angular/core");
const router_1 = require("@angular/router");
const links_1 = require("../../models/links");
let SimpleComponent = class SimpleComponent {
    constructor(router) {
        this.router = router;
        this.text = 'Добро пожаловать!';
        this.links = links_1.LINKS;
    }
    ngOnInit() {
        let link = this.router.url;
        if (link != '/home') {
            this.links.forEach((value) => {
                if (value['link'] === link) {
                    this.text = value['title'];
                }
            });
        }
    }
};
SimpleComponent = __decorate([
    core_1.Component({
        selector: 'simple-page',
        templateUrl: 'app/components/simple/simple.component.html',
        styleUrls: ['app/components/simple/simple.component.css'],
    }),
    __metadata("design:paramtypes", [router_1.Router])
], SimpleComponent);
exports.SimpleComponent = SimpleComponent;
//# sourceMappingURL=simple.component.js.map