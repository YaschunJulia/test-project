import { Component, OnInit} from '@angular/core';
import { Router } from '@angular/router';

import { LINKS } from '../../models/links'

@Component({
  selector: 'simple-page',
  templateUrl: 'app/components/simple/simple.component.html',
  styleUrls: ['app/components/simple/simple.component.css'],
})

export class SimpleComponent implements OnInit {
  text: string = 'Добро пожаловать!';
  links: Object[] = LINKS;

  constructor ( private router: Router) {}

  ngOnInit() {
      let link = this.router.url;
      if (link != '/home') {
          this.links.forEach( (value: Object) => {
              if (value['link'] === link){
                  this.text = value['title'];
              }
          });
      }
  }

}