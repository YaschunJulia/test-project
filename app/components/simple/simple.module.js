"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
/**
 * Created by yaschun on 11.04.17.
 */
const core_1 = require("@angular/core");
const common_1 = require("@angular/common");
const http_1 = require("@angular/http");
const forms_1 = require("@angular/forms");
const simple_component_1 = require("./simple.component");
const simple_routing_module_1 = require("./simple-routing.module");
const bread_crumbs_module_1 = require("../bread_crumbs/bread-crumbs.module");
let SimpleModule = class SimpleModule {
};
SimpleModule = __decorate([
    core_1.NgModule({
        imports: [
            common_1.CommonModule,
            http_1.HttpModule,
            forms_1.FormsModule,
            simple_routing_module_1.SimpleRoutingModule,
            bread_crumbs_module_1.BreadCrumbsModule
        ],
        declarations: [
            simple_component_1.SimpleComponent,
        ],
        bootstrap: [simple_component_1.SimpleComponent],
    })
], SimpleModule);
exports.SimpleModule = SimpleModule;
//# sourceMappingURL=simple.module.js.map