/**
 * Created by yaschun on 11.04.17.
 */
import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";
import { HttpModule } from "@angular/http";
import { FormsModule } from "@angular/forms";

import { Ng2Bs3ModalModule } from 'ng2-bs3-modal/ng2-bs3-modal';
import { InMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService }  from '../../services/in-memory-data.service';

import { ActiveQueryTableComponent }   from '../active_query_table/active-query-table.component';
import { ActiveQueryComponent }   from './active-query.component';
import { ActiveQueryRoutingModule }   from './active-query-routing.module';
import { EditQueryComponent } from '../edit_query/edit-query.component';
import { BreadCrumbsModule }   from '../bread_crumbs/bread-crumbs.module';

import { QueriesService } from '../../services/queries.service';

@NgModule({
    imports:      [
        CommonModule,
        HttpModule,
        FormsModule,
        Ng2Bs3ModalModule,
        InMemoryWebApiModule.forRoot(InMemoryDataService),
        ActiveQueryRoutingModule,
        BreadCrumbsModule],
    declarations: [
        ActiveQueryComponent,
        ActiveQueryTableComponent,
        EditQueryComponent
    ],
    bootstrap:    [ ActiveQueryComponent ],
    providers:    [ QueriesService],
})
export class ActiveQueryModule { }