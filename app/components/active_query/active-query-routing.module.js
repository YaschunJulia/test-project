"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
/**
 * Created by yaschun on 11.04.17.
 */
const core_1 = require("@angular/core");
const router_1 = require("@angular/router");
const active_query_component_1 = require("./active-query.component");
const active_query_table_component_1 = require("../active_query_table/active-query-table.component");
const edit_query_component_1 = require("../edit_query/edit-query.component");
const routes = [
    { path: '',
        component: active_query_component_1.ActiveQueryComponent,
        children: [
            { path: '', component: active_query_table_component_1.ActiveQueryTableComponent },
            { path: ':id', component: edit_query_component_1.EditQueryComponent },
            { path: 'create-new', component: edit_query_component_1.EditQueryComponent }
        ]
    }
];
let ActiveQueryRoutingModule = class ActiveQueryRoutingModule {
};
ActiveQueryRoutingModule = __decorate([
    core_1.NgModule({
        imports: [router_1.RouterModule.forChild(routes)],
        exports: [router_1.RouterModule]
    })
], ActiveQueryRoutingModule);
exports.ActiveQueryRoutingModule = ActiveQueryRoutingModule;
//# sourceMappingURL=active-query-routing.module.js.map