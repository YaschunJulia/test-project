"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
/**
 * Created by yaschun on 11.04.17.
 */
const core_1 = require("@angular/core");
const common_1 = require("@angular/common");
const http_1 = require("@angular/http");
const forms_1 = require("@angular/forms");
const ng2_bs3_modal_1 = require("ng2-bs3-modal/ng2-bs3-modal");
const angular_in_memory_web_api_1 = require("angular-in-memory-web-api");
const in_memory_data_service_1 = require("../../services/in-memory-data.service");
const active_query_table_component_1 = require("../active_query_table/active-query-table.component");
const active_query_component_1 = require("./active-query.component");
const active_query_routing_module_1 = require("./active-query-routing.module");
const edit_query_component_1 = require("../edit_query/edit-query.component");
const bread_crumbs_module_1 = require("../bread_crumbs/bread-crumbs.module");
const queries_service_1 = require("../../services/queries.service");
let ActiveQueryModule = class ActiveQueryModule {
};
ActiveQueryModule = __decorate([
    core_1.NgModule({
        imports: [
            common_1.CommonModule,
            http_1.HttpModule,
            forms_1.FormsModule,
            ng2_bs3_modal_1.Ng2Bs3ModalModule,
            angular_in_memory_web_api_1.InMemoryWebApiModule.forRoot(in_memory_data_service_1.InMemoryDataService),
            active_query_routing_module_1.ActiveQueryRoutingModule,
            bread_crumbs_module_1.BreadCrumbsModule
        ],
        declarations: [
            active_query_component_1.ActiveQueryComponent,
            active_query_table_component_1.ActiveQueryTableComponent,
            edit_query_component_1.EditQueryComponent
        ],
        bootstrap: [active_query_component_1.ActiveQueryComponent],
        providers: [queries_service_1.QueriesService],
    })
], ActiveQueryModule);
exports.ActiveQueryModule = ActiveQueryModule;
//# sourceMappingURL=active-query.module.js.map