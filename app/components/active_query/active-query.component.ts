/**
 * Created by yaschun on 11.04.17.
 */
import { Component }   from '@angular/core';

@Component({
    template: `<router-outlet></router-outlet>`
})
export class ActiveQueryComponent {

    constructor() {}
}