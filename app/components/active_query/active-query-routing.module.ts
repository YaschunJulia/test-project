/**
 * Created by yaschun on 11.04.17.
 */
import { NgModule }     from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ActiveQueryComponent } from './active-query.component';
import { ActiveQueryTableComponent } from '../active_query_table/active-query-table.component';
import { EditQueryComponent } from '../edit_query/edit-query.component';

const routes: Routes = [
    { path: '',
        component: ActiveQueryComponent,
        children: [
            { path: '',    component: ActiveQueryTableComponent },
            { path: ':id', component: EditQueryComponent },
            { path: 'create-new', component: EditQueryComponent }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ActiveQueryRoutingModule {}