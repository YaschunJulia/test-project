/**
 * Created by yaschun on 11.04.17.
 */
import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";

import { BreadCrumbsComponent }   from '../bread_crumbs/bread-crumbs.component';

import { QueriesService } from '../../services/queries.service';

@NgModule({
    imports:      [
        CommonModule,
        FormsModule],
    declarations: [
        BreadCrumbsComponent
    ],
    exports: [ BreadCrumbsComponent ],
    bootstrap: [ BreadCrumbsComponent ],
    providers: [ QueriesService ],
})
export class BreadCrumbsModule { }