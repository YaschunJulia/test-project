"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
const core_1 = require("@angular/core");
const router_1 = require("@angular/router");
const links_1 = require("../../models/links");
const queries_service_1 = require("../../services/queries.service");
let BreadCrumbsComponent = class BreadCrumbsComponent {
    constructor(router, queriesService) {
        this.router = router;
        this.queriesService = queriesService;
        this.links = links_1.LINKS;
    }
    // Find link and create sequence of path
    ngOnInit() {
        let link = this.router.url;
        let linkArray = link.split('/');
        this.crumbs = [{ title: 'Старт', link: '/home' }];
        if (link != '/home') {
            this.links.forEach((value) => {
                if (value['link'] === link || (linkArray.length > 2 && `/${linkArray[1]}` === value['link'])) {
                    this.crumbs.push(value);
                }
            });
        }
        if (linkArray.length > 2 && isNaN(+linkArray[2]) === false) {
            let serviceName = this.queriesService.serviceName || window.localStorage.getItem('service-name');
            this.crumbs.push({ title: serviceName, link: link });
        }
    }
    goToLink(crumb, crumbs, i) {
        if (crumbs.length > 1) {
            crumbs.splice(i + 1);
            this.router.navigate([crumb['link']]);
        }
    }
};
BreadCrumbsComponent = __decorate([
    core_1.Component({
        selector: 'breadcrumbs',
        templateUrl: 'app/components/bread_crumbs/bread-crumbs.component.html',
        styleUrls: ['app/components/bread_crumbs/bread-crumbs.component.css']
    }),
    __metadata("design:paramtypes", [router_1.Router,
        queries_service_1.QueriesService])
], BreadCrumbsComponent);
exports.BreadCrumbsComponent = BreadCrumbsComponent;
//# sourceMappingURL=bread-crumbs.component.js.map