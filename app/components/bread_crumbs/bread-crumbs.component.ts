import { Component, OnInit} from '@angular/core';
import { Router } from '@angular/router';

import { LINKS } from '../../models/links'

import { QueriesService } from '../../services/queries.service';

@Component({
  selector: 'breadcrumbs',
  templateUrl: 'app/components/bread_crumbs/bread-crumbs.component.html',
  styleUrls: ['app/components/bread_crumbs/bread-crumbs.component.css']
})

export class BreadCrumbsComponent implements OnInit{
  crumbs: Object[];
  links: Object[] = LINKS;


  constructor (private router: Router,
               private queriesService: QueriesService) {}

  // Find link and create sequence of path
  ngOnInit() {
    let link = this.router.url;
    let linkArray = link.split('/') as string[];
    this.crumbs = [{title: 'Старт', link: '/home'}];
    if (link != '/home') {
      this.links.forEach((value: Object) => {
        if (value['link'] === link || (linkArray.length > 2 && `/${linkArray[1]}` === value['link'])) {
          this.crumbs.push(value);
        }
      })
    }
    if (linkArray.length > 2 && isNaN(+linkArray[2]) === false) {
      let serviceName = this.queriesService.serviceName || window.localStorage.getItem('service-name');
      this.crumbs.push({title: serviceName, link: link});
    }

  }

  goToLink(crumb: Object, crumbs: Object[], i: number) {
    if (crumbs.length > 1) {
      crumbs.splice(i + 1);
      this.router.navigate([crumb['link']])
    }
  }
}