import { Component, OnInit, ViewChild} from '@angular/core';
import { Router, ActivatedRoute, Params} from '@angular/router';
import { ModalComponent } from 'ng2-bs3-modal/ng2-bs3-modal';

import { QueriesService } from '../../services/queries.service';

import { Service } from '../../models/service.model';

@Component({
  selector: 'active-query',
  templateUrl: 'app/components/edit_query/edit-query.component.html',
  styleUrls: ['app/components/edit_query/edit-query.component.css']
})

export class EditQueryComponent implements OnInit{
  id: number;
  service: Service = new Service( 0, 'Новая услуга', 100, false, 'Активная', '', null, null, '');
  fieldError: string;

  @ViewChild('createTemplateModal') modal: ModalComponent;

  constructor (
      private router: Router,
      private route: ActivatedRoute,
      private queriesService: QueriesService
  ) {}

  ngOnInit() {
    this.route.params.forEach((params: Params) => {
      if (!isNaN(+params['id'])) {
        this.id = +params['id'];
        this.getData()
      }
    });
  }

  getData() {
    this.queriesService.getService(this.id)
        .then((service: Service) => {
          this.service = service;
        })
        .catch( () => {})
  }

  cancelSaveField() {
    this.router.navigate(['./active-queries'])
  }

  // Check form valid and show error or save service information
  saveField(service: Service, valid: boolean) {
    if ( valid ) {
      if (service.description && service.description.length < 30) {
        this.fieldError = 'Поле описание должно быть либо пустым, либо содержать более 30ти символов';
      } else {
        if ( service.id != 0) {
          this.queriesService.update(service)
              .then(() => this.router.navigate(['./active-queries']));
        } else {
          this.queriesService.create(service)
              .then(() => this.router.navigate(['./active-queries']));
        }
      }
    } else {
      this.fieldError = 'Все поля, кроме описания являются обязательными'
    }

  }



}