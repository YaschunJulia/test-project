"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
const core_1 = require("@angular/core");
const router_1 = require("@angular/router");
const ng2_bs3_modal_1 = require("ng2-bs3-modal/ng2-bs3-modal");
const queries_service_1 = require("../../services/queries.service");
const service_model_1 = require("../../models/service.model");
let EditQueryComponent = class EditQueryComponent {
    constructor(router, route, queriesService) {
        this.router = router;
        this.route = route;
        this.queriesService = queriesService;
        this.service = new service_model_1.Service(0, 'Новая услуга', 100, false, 'Активная', '', null, null, '');
    }
    ngOnInit() {
        this.route.params.forEach((params) => {
            if (!isNaN(+params['id'])) {
                this.id = +params['id'];
                this.getData();
            }
        });
    }
    getData() {
        this.queriesService.getService(this.id)
            .then((service) => {
            this.service = service;
        })
            .catch(() => { });
    }
    cancelSaveField() {
        this.router.navigate(['./active-queries']);
    }
    // Check form valid and show error or save service information
    saveField(service, valid) {
        if (valid) {
            if (service.description && service.description.length < 30) {
                this.fieldError = 'Поле описание должно быть либо пустым, либо содержать более 30ти символов';
            }
            else {
                if (service.id != 0) {
                    this.queriesService.update(service)
                        .then(() => this.router.navigate(['./active-queries']));
                }
                else {
                    this.queriesService.create(service)
                        .then(() => this.router.navigate(['./active-queries']));
                }
            }
        }
        else {
            this.fieldError = 'Все поля, кроме описания являются обязательными';
        }
    }
};
__decorate([
    core_1.ViewChild('createTemplateModal'),
    __metadata("design:type", ng2_bs3_modal_1.ModalComponent)
], EditQueryComponent.prototype, "modal", void 0);
EditQueryComponent = __decorate([
    core_1.Component({
        selector: 'active-query',
        templateUrl: 'app/components/edit_query/edit-query.component.html',
        styleUrls: ['app/components/edit_query/edit-query.component.css']
    }),
    __metadata("design:paramtypes", [router_1.Router,
        router_1.ActivatedRoute,
        queries_service_1.QueriesService])
], EditQueryComponent);
exports.EditQueryComponent = EditQueryComponent;
//# sourceMappingURL=edit-query.component.js.map