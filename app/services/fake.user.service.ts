/**
 * Created by yaschun on 10.04.17.
 */

import { User } from '../models/user.model';
export { User } from '../models/user.model';

export const USER: User = new User('Yulia', 30, 'bonus');

export class FakeUserService {
    user: User = USER.clone();
    lastPromise: Promise<any>;

    getUserInfo() : Promise<Object> {
        return this.lastPromise = Promise.resolve<Object>(this.user);
    }
}