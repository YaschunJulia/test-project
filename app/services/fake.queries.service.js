"use strict";
/**
 * Created by yaschun on 10.04.17.
 */
const service_model_1 = require("../models/service.model");
var service_model_2 = require("../models/service.model");
exports.Service = service_model_2.Service;
exports.SERVICES = [
    new service_model_1.Service(1, 'Услуга 1', 100, false, 'Активная', '', new Date(), new Date(), ''),
    new service_model_1.Service(2, 'Услуга 2', 200, false, 'Активная', '', new Date(), new Date(), '')
];
class QueriesService {
    constructor() {
        this.services = exports.SERVICES.map(i => i.clone());
    }
    getServices() {
        return this.lastPromise = Promise.resolve(this.services);
    }
    getService(id) {
        return this.lastPromise = Promise.resolve(this.services[id]);
    }
    create(service) {
        return this.lastPromise = Promise.resolve(service);
    }
    update(service) {
        return this.lastPromise = Promise.resolve(service);
    }
    deleteService(id) {
        return this.lastPromise = Promise.resolve(null);
    }
}
exports.QueriesService = QueriesService;
//# sourceMappingURL=fake.queries.service.js.map