"use strict";
class InMemoryDataService {
    createDb() {
        let user_info = { name: 'Андрей Рублев', count: '500 РУБ.', bonus: 'бонус на первую покупку' };
        let services = [
            { id: 1, name: 'Услуга 1', count: 100, deleted: false, status: 'Активная',
                description: '', activateFrom: '2017-04-10', activateTo: '2017-05-20', comment: '' },
            { id: 5, name: 'Услуга 2', count: 1000, deleted: false, status: 'Активная',
                description: '', activateFrom: '2017-04-10', activateTo: '2017-05-20', comment: '' },
            { id: 10, name: 'Услуга 3', count: 5500, deleted: false, status: 'Активная',
                description: '', activateFrom: '2017-04-10', activateTo: '2017-05-20', comment: '' },
            { id: 14, name: 'Услуга 4', count: 18500, deleted: false, status: 'Активная',
                description: '', activateFrom: '2017-04-10', activateTo: '2017-05-20', comment: '' },
        ];
        return { user_info, services };
    }
}
exports.InMemoryDataService = InMemoryDataService;
//# sourceMappingURL=in-memory-data.service.js.map