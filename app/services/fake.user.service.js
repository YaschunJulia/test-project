/**
 * Created by yaschun on 10.04.17.
 */
"use strict";
const user_model_1 = require("../models/user.model");
var user_model_2 = require("../models/user.model");
exports.User = user_model_2.User;
exports.USER = new user_model_1.User('Yulia', 30, 'bonus');
class FakeUserService {
    constructor() {
        this.user = exports.USER.clone();
    }
    getUserInfo() {
        return this.lastPromise = Promise.resolve(this.user);
    }
}
exports.FakeUserService = FakeUserService;
//# sourceMappingURL=fake.user.service.js.map