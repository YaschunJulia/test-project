/**
 * Created by yaschun on 07.04.17.
 */
import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { Service } from '../models/service.model';

@Injectable()
export class QueriesService {
    private servicesUrl = 'app/services';  // URL to web api
    private headers = new Headers({'Content-Type': 'application/json'});
    serviceName: string;

    constructor(private http: Http) { }

    getServices(): Promise<Service[]> {
        return this.http.get(this.servicesUrl)
            .toPromise()
            .then(response => response.json().data as Service[])
            .catch(this.handleError);
    }

    getService(id: number): Promise<Service> {
        return this.getServices()
            .then((services : Service[]) => services.find((service : Service) => service.id === id));
    }

    // sort(field: string): Promise<Service[]> {
    //     return this.getServices()
    //         .then((services : Service[]) => {
    //             return services;
    //         });
    // }

    create(service: Service): Promise<Service> {
        return this.getServices()
            .then((services : Service[]) => {
            let max = 0;
                services.forEach( (s : Service) => {
                    if (s.id > max) {
                        max = s.id;
                    }
                })
                service.id = max +1;
                return service as Service;
            })
            .then( (service: Service) => {
                return this.http
                    .post(this.servicesUrl, JSON.stringify(service), {headers: this.headers})
                    .toPromise()
                    .then(res => res.json().data as Service)
                    .catch(this.handleError);
            })
    }

    update(service: Service): Promise<Service> {
        const url = `${this.servicesUrl}/${service.id}`;
        return this.http
            .put(url, JSON.stringify(service), {headers: this.headers})
            .toPromise()
            .then(() => service)
            .catch(this.handleError);
    }

    deleteService(id: number): Promise<void> {
        const url = `${this.servicesUrl}/${id}`;
        return this.http.delete(url, {headers: this.headers})
            .toPromise()
            .then(() => null)
            .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred' + error.status + '' + error.message);
        return Promise.reject(error.message || error);
    }
}