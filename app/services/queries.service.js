"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/**
 * Created by yaschun on 07.04.17.
 */
const core_1 = require("@angular/core");
const http_1 = require("@angular/http");
require("rxjs/add/operator/toPromise");
let QueriesService = class QueriesService {
    constructor(http) {
        this.http = http;
        this.servicesUrl = 'app/services'; // URL to web api
        this.headers = new http_1.Headers({ 'Content-Type': 'application/json' });
    }
    getServices() {
        return this.http.get(this.servicesUrl)
            .toPromise()
            .then(response => response.json().data)
            .catch(this.handleError);
    }
    getService(id) {
        return this.getServices()
            .then((services) => services.find((service) => service.id === id));
    }
    // sort(field: string): Promise<Service[]> {
    //     return this.getServices()
    //         .then((services : Service[]) => {
    //             return services;
    //         });
    // }
    create(service) {
        return this.getServices()
            .then((services) => {
            let max = 0;
            services.forEach((s) => {
                if (s.id > max) {
                    max = s.id;
                }
            });
            service.id = max + 1;
            return service;
        })
            .then((service) => {
            return this.http
                .post(this.servicesUrl, JSON.stringify(service), { headers: this.headers })
                .toPromise()
                .then(res => res.json().data)
                .catch(this.handleError);
        });
    }
    update(service) {
        const url = `${this.servicesUrl}/${service.id}`;
        return this.http
            .put(url, JSON.stringify(service), { headers: this.headers })
            .toPromise()
            .then(() => service)
            .catch(this.handleError);
    }
    deleteService(id) {
        const url = `${this.servicesUrl}/${id}`;
        return this.http.delete(url, { headers: this.headers })
            .toPromise()
            .then(() => null)
            .catch(this.handleError);
    }
    handleError(error) {
        console.error('An error occurred' + error.status + '' + error.message);
        return Promise.reject(error.message || error);
    }
};
QueriesService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http])
], QueriesService);
exports.QueriesService = QueriesService;
//# sourceMappingURL=queries.service.js.map