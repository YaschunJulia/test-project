/**
 * Created by yaschun on 10.04.17.
 */
import { Service } from '../models/service.model';
export { Service } from '../models/service.model';

export const SERVICES: Service[] = [
    new Service( 1, 'Услуга 1', 100, false, 'Активная', '', new Date(), new Date(),''),
    new Service(2, 'Услуга 2', 200, false, 'Активная', '', new Date(), new Date(),'')
]

export class QueriesService {
    services = SERVICES.map(i => i.clone());
    lastPromise: Promise<any>;

    getServices(): Promise<Service[]> {
        return this.lastPromise = Promise.resolve<Object>(this.services);
    }

    getService(id: number): Promise<Service> {
        return this.lastPromise = Promise.resolve<Object>(this.services[id]);
    }

    create(service: Service): Promise<Service> {
        return this.lastPromise = Promise.resolve<Object>(service);
    }

    update(service: Service): Promise<Service> {
        return this.lastPromise = Promise.resolve<Object>(service);
    }

    deleteService(id: number): Promise<void> {
        return this.lastPromise = Promise.resolve(null);
    }
}