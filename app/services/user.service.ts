/**
 * Created by yaschun on 07.04.17.
 */
import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { User } from '../models/user.model';

@Injectable()
export class UserService {
    private userUrl = 'app/user_info';  // URL to web api

    constructor(private http: Http) { }

    getUserInfo(): Promise<User> {
        return this.http.get(this.userUrl)
            .toPromise()
            .then(response => response.json().data as User)
            .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred' + error.status + '' + error.message); // for demo purposes only
        return Promise.reject(error.message || error);
    }
}